// For loading spinner
$body = $("body");
$(document).on({
    ajaxStart:function(){$body.addClass("loading");},
    ajaxStop:function(){$body.removeClass("loading");}
});

$( document ).ready(function(e) {

  // Setup data table
  var weatherTable = $('#weather-table').DataTable({
    "searching":false,
    "ordering":false,
    "paging":false,
    "lengthChange":false,
    "bInfo" : false,
    "pageLenght":100,
    "columnDefs": [
      {"className": "dt-center", "targets": "_all"}
    ],
  });

  // On click gets and displays weather data
  $( "#btnGetWeather" ).on( "click", function(e) {
    e.preventDefault();

    // Location
    var location = $("#location").val();

    // Clear table before displaying new results
    weatherTable.clear();

    // Hide error message
    if(!$("#error").hasClass("hide")){
          $("#error").addClass("hide");
    }

    // Ajax call to get weather data
    $.get("weather", {location: location}, function(result) {
      if(result["status"] == "OK"){
        // Iterate through each row and add to data table
        $.each(result["weather"], function( index, value ) {
          weatherTable.row.add([
              value["date"],
              value["low"],
              value["high"],
              value["precipitationProbability"],
              value["uvIndex"]
          ]).draw(false);
        });
      } else {
        $("#error").empty().text(result["message"]);
        $("#error").removeClass("hide");
      }
    });
  });

});
