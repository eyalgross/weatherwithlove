<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use GuzzleHttp\Exception\GuzzleException;
use Carbon\Carbon;

class WeatherController extends Controller
{

    /**
     * Orchestrator function for getting weather
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getWeather(Request $request)
    {
      // Get location from UI
      $userLocation = $request->input('location');

      // Get lat and long coordinates from Google Geocoding API from location
      $latLng = $this->getLatLng($userLocation);

      // Return if there was an error from Google Geocoding API
      if ($latLng["status"] != "OK") {
        return [
          "status" => "ERROR",
          "message" => $latLng["message"]
        ];
      }

      // Final array containing all weather values
      $weather = [];

      // Lat and Long data from google
      $lat = $latLng["lat"];
      $lng = $latLng["lng"];

      // Configurable days in the past and future to get
      $limit = config('weatherwithlove.limit');

      // Setup API calls
      $apiCalls = $this->setupAPICalls($limit, $lat, $lng, $weather);

      // Get weather data from Dark Sky API
      $this->getWeatherFromAPI($apiCalls, $weather);

      // Return weather
      return [
          "status" => "OK",
          "weather" => $weather
      ];
    }

    // Get lat and long values from Google Maps Geocoding API
    private function getLatLng ($userLocation) {
      // Make call to Google Maps Geocoding API
      $googleResponse = json_decode(\GoogleMaps::load('geocoding')
        ->setParam (['address' => $userLocation])
        ->get());

      // Get status from response and check for error, return if error found
      $googleStatus = $googleResponse->status;
      if ($googleStatus != "OK") {
        if ($googleStatus == "ZERO_RESULTS") {
          return [
            "status" => "ERROR",
            "message" => "Could not find that location. Please try again."
          ];
        } else {
          return [
            "status" => "ERROR",
            "message" => "There was an error searching for that location. Please try again."
          ];
        }
      }

      // Get lat and long values from response
      $googleLocation = $googleResponse->results[0]->geometry->location;
      $lat = $googleLocation->lat;
      $lng = $googleLocation->lng;

      // Return lat and long values
      return [
        "status" => "OK",
        "lat" => $lat,
        "lng" => $lng
      ];
    }

    // Get weather from Dark Sky API for a particulare day
    private function setupAPICalls ($limit, $lat, $lng, &$weather) {
      // Setup client, promises and API key
      $client = new Client();
      $apiCalls = [];
      $apiKey = env('DARKSKY_API_KEY', false);

      // Setup API calls to get weather from the past including today
      for ($i = $limit; $i >= 0; $i--){
        // Date to pass into call to Dark Sky API
        $weatherDate = Carbon::now()->subDays($i);

        // Format date for response
        $weatherDateFormatted = $weatherDate->format("d/m/Y");

        // Get UNIX time value
        $weatherTimestamp = $weatherDate->timestamp;

        // Create array of promises for concurrent API requests
        $apiCalls[] = $client->requestAsync('GET', 'https://api.darksky.net/forecast/'.$apiKey.'/'.$lat.','.$lng.','.$weatherTimestamp.'?excludes=currently,minutely,hourly,alerts,flags&units=ca');
      }

      // Setup API calls to get weather from the future
      for ($i = 1; $i <= $limit; $i++){
        // Date to pass into call to Dark Sky API
        $weatherDate = Carbon::now()->addDays($i);

        // Format date for response
        $weatherDateFormatted = $weatherDate->format("d/m/Y");

        // Get UNIX time value
        $weatherTimestamp = $weatherDate->timestamp;

        // Create array of promises for concurrent API requests
        $apiCalls[] = $client->requestAsync('GET', 'https://api.darksky.net/forecast/'.$apiKey.'/'.$lat.','.$lng.','.$weatherTimestamp.'?excludes=currently,minutely,hourly,alerts,flags&units=ca');
      }

      return $apiCalls;
    }

    // Make concurrent calls to Dark Sky API storing response values when returned
    function getWeatherFromAPI($apiCalls, &$weather) {
      Promise\all($apiCalls)->then(function (array $responses) use (&$weather) {
          foreach ($responses as $response) {
            // Format response
            $responseBody = json_decode($response->getBody()->getContents(), true);

            // Get Daily data
            $dailyData = $responseBody["daily"]["data"][0];

            //Get formatted date
            $weatherDate = new Carbon();
            $weatherDate->timestamp = $dailyData["time"];
            $weatherDateFormatted = $weatherDate->format("d/m/Y");

            // Set array of values for displaying on UI
            $dayWeather = [
             "date" => $weatherDateFormatted,
             "low" => number_format(round($dailyData["temperatureLow"]),0),
             "high" => number_format(round($dailyData["temperatureHigh"]),0),
             "precipitationProbability" => !empty($dailyData["precipProbability"]) ? number_format($dailyData["precipProbability"] * 100, 0) : "-",
             "uvIndex" => !empty($dailyData["uvIndex"]) ? $dailyData["uvIndex"] : "-"
            ];

            // Push values to final array
            array_push($weather, $dayWeather);
          }
      })->wait();
    }
}
