# Weather With Love

## Description

An application that allows the user to view the observed (in the past 30 days) or forecasted (in the future) daily weather conditions for a given location using the Dark Sky API.

## Requirements

- Use PHP
- Send multiple requests concurrently

## Solution Technology

- Laravel 5.6
- Google Maps Geocoding API for retrieving Lat / Long values with Laravel Google Maps package: https://github.com/alexpechkarev/google-maps
- Guzzle for concurrent Dark Sky API calls: https://github.com/guzzle/guzzle
- Carbon for dates: https://github.com/briannesbitt/carbon
- jQuery

## Files of interest

- app/Http/Controllers/WeatherController
- resources/views/weather.blade.php
- public/js/weatherwithlove.js
- public/css/weatherwithlove.css

## URL of Live Application

http://188.166.58.25/

