<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Weather with Love</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Datatables -->
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">

    <!-- Custom css -->
    <link rel="stylesheet" href="{{ asset('css/weatherwithlove.css') }}">
  </head>

  <body>

    <div class="container">

      <div class="row">
        <div class="span12">
          <h2>Weather With Love</h2>
        </div>
      </div>
      <div class="row">
        <div class="span12">
          <form id="search-form" class="form-inline">
            @csrf
            <div class="form-group">
              <input type="text" class="form-control" id="location" placeholder="Location" name="location">
            </div>
            <button type="submit" class="btn btn-default" id="btnGetWeather">Get Weather</button>
            <label id="error" class="hide"></label>
          </form>
        </div>
      </div>
      <div class="row">
        <div class="span12">
          <table id="weather-table" class="display">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Low (°C)</th>
                    <th>High (°C)</th>
                    <th>Precipitation Probability (%)</th>
                    <th>UV Index</th>
                </tr>
            </thead>
        </table>
        </div>
      </div>

    </div><!-- /.container -->

    <div class="modal"></div>

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <!-- Bootstrap -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- Datatables -->
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

    <!-- Custom js -->
    <script src="{{ asset('js/weatherwithlove.js') }}"></script>
  </body>
</html>
